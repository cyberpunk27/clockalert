import React, { Component } from 'react';
import logo from './logo.svg';
import './App.css';
import Clock from "./Clock";
import Header from "./Header";
import CountDown from "./CountDown";

class App extends Component {
  render() {
    return (
        <div>
          <Header/>
          <CountDown/>
        </div>

    );
  }
}

export default App;
