/**
 * Created by hp on 2/19/2019.
 */
import './Clock.css';
import React, { Component } from 'react';
import ReactDOM from 'react-dom';
class Clock extends Component {


    constructor(props){

        super(props);

        this.state = {
            date: new Date(),
       }

    }

    componentDidMount(){
        this.setState({
            hours : this.state.date.getHours(),
            hoursAngle : this.state.date.getHours() / 12 * 360,
            minutes : this.state.date.getMinutes(),
            minutesAngle : this.state.date.getHours() * 360 + this.state.date.getMinutes() / 60 * 360,
            seconds : this.state.date.getSeconds(),
            secondsAngle : this.state.date.getHours() * 360 + this.state.date.getMinutes() * 360 + this.state.date.getSeconds() / 60 * 360,
        });
        const node = ReactDOM.findDOMNode(this);
        this.clock = node.querySelector('.clock');
        this.needleHours   =  node.querySelector('.needle.hours')
        this.needleMinutes =  node.querySelector('.needle.minutes')
        this.needleSeconds = node.querySelector('.needle.seconds')

        this.timerID = setInterval( () => {this.tick()}, 1000)
        // Styles
     }

    tick(){
        this.setState({
            date: new Date(),
            hours : this.state.date.getHours(),
            hoursAngle : this.state.date.getHours() / 12 * 360,
            minutes : this.state.date.getMinutes(),
            minutesAngle : this.state.date.getHours() * 360 + this.state.date.getMinutes() / 60 * 360,
            seconds : this.state.date.getSeconds(),
            secondsAngle : this.state.date.getHours() * 360 + this.state.date.getMinutes() * 360 + this.state.date.getSeconds() / 60 * 360,
        });
        this.needleHours.style.transform   = `rotate(${this.state.hoursAngle + (this.state.minutesAngle % 360) / 12}deg)`
        this.needleMinutes.style.transform = `rotate(${this.state.minutesAngle}deg)`
        this.needleSeconds.style.transform = `rotate(${this.state.secondsAngle}deg)`

        this.needleHours.style.boxShadow   = `${2.5 * Math.sin(((this.state.hoursAngle + 45) % 360) * 2 * Math.PI / 360)}px ${2.5 * Math.cos(((this.state.hoursAngle + 45) % 360) * 2 * Math.PI / 360)}px 2.5px rgba(0, 0, 0, 0.75)`
        this.needleMinutes.style.boxShadow = `${2.5 * Math.sin(((this.state.minutesAngle + 45) % 360) * 2 * Math.PI / 360)}px ${2.5 * Math.cos(((this.state.minutesAngle + 45) % 360) * 2 * Math.PI / 360)}px 2.5px rgba(0, 0, 0, 0.75)`
        this.needleSeconds.style.boxShadow = `${2.5 * Math.sin(((this.state.secondsAngle + 45) % 360) * 2 * Math.PI / 360)}px ${2.5 * Math.cos(((this.state.secondsAngle + 45) % 360) * 2 * Math.PI / 360)}px 2.5px rgba(0, 0, 0, 0.75)`

    }

    componentWillUnmount(){
        clearInterval(this.timerID);
    }
    render(){
        return (<div className="clock">
            <div className="axe">
                <div className="needle hours"></div>
                <div className="needle minutes"></div>
                <div className="needle seconds"></div>
                <div className="needle screw"></div>
            </div>
            <div className="numbers">
                <div className="number number1">1</div>
                <div className="number number2">2</div>
                <div className="number number3">3</div>
                <div className="number number4">4</div>
                <div className="number number5">5</div>
                <div className="number number6">6</div>
                <div className="number number7">7</div>
                <div className="number number8">8</div>
                <div className="number number9">9</div>
                <div className="number number10">10</div>
                <div className="number number11">11</div>
                <div className="number number12">12</div>
            </div>
            <div className="dots">
                <div className="space space1">
                    <div className="dot dot1"></div>
                    <div className="dot dot2"></div>
                    <div className="dot dot3"></div>
                    <div className="dot dot4"></div>
                </div>
                <div className="space space2">
                    <div className="dot dot1"></div>
                    <div className="dot dot2"></div>
                    <div className="dot dot3"></div>
                    <div className="dot dot4"></div>
                </div>
                <div className="space space3">
                    <div className="dot dot1"></div>
                    <div className="dot dot2"></div>
                    <div className="dot dot3"></div>
                    <div className="dot dot4"></div>
                </div>
                <div className="space space4">
                    <div className="dot dot1"></div>
                    <div className="dot dot2"></div>
                    <div className="dot dot3"></div>
                    <div className="dot dot4"></div>
                </div>
                <div className="space space5">
                    <div className="dot dot1"></div>
                    <div className="dot dot2"></div>
                    <div className="dot dot3"></div>
                    <div className="dot dot4"></div>
                </div>
                <div className="space space6">
                    <div className="dot dot1"></div>
                    <div className="dot dot2"></div>
                    <div className="dot dot3"></div>
                    <div className="dot dot4"></div>
                </div>
                <div className="space space7">
                    <div className="dot dot1"></div>
                    <div className="dot dot2"></div>
                    <div className="dot dot3"></div>
                    <div className="dot dot4"></div>
                </div>
                <div className="space space8">
                    <div className="dot dot1"></div>
                    <div className="dot dot2"></div>
                    <div className="dot dot3"></div>
                    <div className="dot dot4"></div>
                </div>
                <div className="space space9">
                    <div className="dot dot1"></div>
                    <div className="dot dot2"></div>
                    <div className="dot dot3"></div>
                    <div className="dot dot4"></div>
                </div>
                <div className="space space10">
                    <div className="dot dot1"></div>
                    <div className="dot dot2"></div>
                    <div className="dot dot3"></div>
                    <div className="dot dot4"></div>
                </div>
                <div className="space space11">
                    <div className="dot dot1"></div>
                    <div className="dot dot2"></div>
                    <div className="dot dot3"></div>
                    <div className="dot dot4"></div>
                </div>
                <div className="space space12">
                    <div className="dot dot1"></div>
                    <div className="dot dot2"></div>
                    <div className="dot dot3"></div>
                    <div className="dot dot4"></div>
                </div>
            </div>
            <svg className="border side" height="500" width="500">
                <circle cx="250" cy="250" r="200" stroke="#333" strokeWidth="10" fill="none"/>
            </svg>
            <svg className="border front" height="500" width="500">
                <circle cx="250" cy="250" r="200" stroke="#333" strokeWidth="10" fill="none"/>
            </svg>
        </div>
        )
    }
}

export default Clock;
