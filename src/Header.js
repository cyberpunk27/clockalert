/**
 * Created by hp on 2/19/2019.
 */

import React, { Component } from 'react';
import {Navbar, NavItem} from 'react-materialize';

class Header extends Component {
    render() {
        return (
            <div>
                <Navbar brand='Alert Clock' centerLogo>
                </Navbar>
            </div>
        );
    }
}

export default Header;

