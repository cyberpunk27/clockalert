/**
 * Created by hp on 2/20/2019.
 */
import React, { Component } from 'react';
import {Modal, Button} from 'react-materialize';
import './CountDown.css';
import Countdown from 'react-countdown-now';
import Clock from "./Clock";

const $ = window.$;

class CountDown extends Component {

    constructor(props){
        super(props);
        this.state = {
            time:60000,
            key: Math.random()
        }

        this.resetState = this.resetState.bind(this);

    }


    componentDidMount(){
        let elem = document.getElementById('modal1');
        // let instance = M.Modal.init(elem);


    }

    resetState(){

        console.log('called click');
        this.setState({ key : Math.random()});
        $('#modal1').modal('close');

    }
    render() {
        return (

            <div className="contain">
                <div id="modal1" className="modal">
                    <Clock/>
                    <div className="modal-footer">
                        <a href="#!" className="modal-close waves-effect waves-green btn-flat" onClick={ this.resetState}>CLOSE</a>
                    </div>
                </div>
                <h1 id="head">Clock Will show in :</h1>
                <ul>
                    <li><span id="seconds"></span>
                        <Countdown key={this.state.key} date={Date.now() + this.state.time}>
                        <Completionist />
                    </Countdown>Seconds</li>
                </ul>
            </div>

        );
    }
}
const Completionist = () => {
    $('#modal1').modal();
    $('#modal1').modal('open');
    return (<div>SHOWTIME</div>);
};


export default CountDown;
